<?php

namespace App\Models;

use App\Traits\PublicId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vaga extends Model
{
    use HasFactory, PublicId;

    protected $table = 'vagas';
    protected $fillable = [
        'public_id', 'cargo', 'descricao', 'contratacao', 'status',
    ];

    public function candidatos()
    {
        return $this->belongsToMany(Candidato::class, 'candidatos_rel_vagas', 'vaga_id', 'candidato_id')->withTimestamps();
    }
}
