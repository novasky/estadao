<?php

namespace App\Models;

use App\Traits\PublicId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Candidato extends Authenticatable
{
    use HasFactory, PublicId, Notifiable;

    protected $table = 'candidatos';
    protected $fillable = [
        'public_id', 'nome', 'telefone', 'email', 'email_verified_at',
        'password', 'contratacao', 'status',
    ];

    public function candidaturas()
    {
        return $this->belongsToMany(Vaga::class, 'candidatos_rel_vagas', 'candidato_id', 'vaga_id')->withTimestamps();
    }
}
