<?php

namespace App\Http\Controllers;

use App\Models\Vaga;

class HomeController extends Controller
{
    public function index()
    {
        $vagas = Vaga::orderBy('status', 'DESC')->orderBy('cargo')->paginate(20);
        return view('home', compact('vagas'));
    }
}
