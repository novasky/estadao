<?php

namespace App\Http\Controllers\Candidato\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME_CANDIDATO;

    protected function guard()
    {
        return Auth::guard('candidato');
    }

    public function showResetForm(Request $request)
    {
        $token = $request->route()->parameter('token');

        return view('candidato.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
