<?php

namespace App\Http\Controllers\Candidato\Auth;

use App\Http\Controllers\Controller;
use App\Models\Candidato;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $redirectTo = RouteServiceProvider::HOME_CANDIDATO;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm(Request $request)
    {
        return view('candidato.auth.register');
    }

    protected function guard()
    {
        return Auth::guard('candidato');
    }

    protected function validator(array $data)
    {
        $data['active'] = 1;
        return Validator::make($data, [
            'nome'     => ['required', 'string', 'max:191'],
            'email'    => ['required_without:telefone', 'nullable', 'string', 'email', 'unique:candidatos', 'max:191'],
            'telefone' => ['required_without:email', 'string', 'max:20'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ],[
            'nome.*'          => 'O nome é obrigatório.',
            'email.unique'    => 'O e-mail já está sendo utilizado.',
            'email.*'         => 'O e-mail não é válido.',
            'telefone.unique' => 'O telefone já está sendo utilizado.',
            'telefone.*'      => 'O telefone não é válido.',
            'password.*'      => 'A senha é obrigatória e precisa ter no mínimo 8 caracteres.',
        ]);
    }

    protected function create(array $data)
    {
        $candidato = Candidato::create([
            'nome'     => $data['nome'],
            'email'    => $data['email'],
            'telefone' => $data['telefone'],
            'password' => Hash::make($data['password']),
            'active'   => 1,
        ]);

        return $candidato;
    }
}
