<?php

namespace App\Http\Controllers\Candidato\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME_CANDIDATO;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->username = $this->findUsername();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    // public function findUsername()
    // {
    //     $username = request()->input('username');
        
    //     $fieldType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'telefone';
 
    //     request()->merge([$fieldType => $username]);
 
    //     return $fieldType;
    // }

    /**
     * Get username property.
     *
     * @return string
     */
    // public function username()
    // {
    //     return $this->username;
    // }

    protected function guard()
    {
        return Auth::guard('candidato');
    }

    public function showLoginForm()
    {
        return view('candidato.auth.login');
    }
}
