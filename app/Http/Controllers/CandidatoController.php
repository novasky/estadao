<?php

namespace App\Http\Controllers;

use App\Models\Vaga;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CandidatoController extends Controller
{
    public function candidaturas()
    {
        $candidaturas = auth()->guard('candidato')->user()->candidaturas;
        return view('candidato.candidaturas', compact('candidaturas'));
    }

    public function candidatar(Request $request, $public_id)
    {
        $candidato = auth()->guard('candidato')->user()->id;

        $vaga = Vaga::where('status', 1)->where('public_id', $public_id)->firstOrFail();
        if(!$vaga->candidatos->contains( $candidato )) {
            $vaga->candidatos()->attach($candidato);
        }
        
        Session::flash('flash_message', 'Sua candidatura foi enviada com sucesso!');

        return redirect()->route('home');
    }
}
