<?php

namespace App\Http\Controllers\Painel;

use App\Models\Candidato;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCandidatoRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CandidatosControler extends Controller
{
    protected $view = 'candidatos.';

    public function index(Request $request)
    {
        $order   = $request->filled('order') ? $request->order : 'nome';
        $orderby = $request->filled('orderby') ? $request->orderby : 'asc';
        $candidatos = Candidato::when($request->filled('pesquisar'), function($query) use ($request){
            return $query->where('nome', 'LIKE', '%'.$request->pesquisar.'%')
                ->orWhere('email', 'LIKE', '%'.$request->pesquisar.'%')
                ->orWhere('telefone', 'LIKE', '%'.$request->pesquisar.'%')
                ->orWhere('public_id', 'LIKE', '%'.$request->pesquisar.'%');
        })->orderBy($order, $orderby)->paginate(20);
        return view($this->view . 'index', compact('candidatos'));
    }


    public function create()
    {
        return view($this->view . 'form');
    }


    public function store(CreateCandidatoRequest $request)
    {
        Candidato::create( $request->all() );
        Session::flash('flash_message', 'Candidato salvo com sucesso!');
        return redirect()->route('painel.' . $this->view . 'index');
    }


    public function update(CreateCandidatoRequest $request, Candidato $candidato)
    {
        $candidato->update( $request->all() );
        Session::flash('flash_message', 'Candidato atualizado com sucesso!');
        return redirect()->route('painel.' . $this->view . 'index');
    }


    public function edit(Candidato $candidato)
    {
        return view($this->view . 'form', compact('candidato'));
    }
 

    public function destroy(Candidato $candidato)
    {
        $candidato->delete();
        Session::flash('flash_message', 'Candidato removido com sucesso!');
        return redirect()->route('painel.' . $this->view . 'index');
    }
 

    public function destroyMass(Request $request)
    {
        $checados = explode(',', trim($request->checados, ','));

        foreach($checados as $c) {
            Candidato::where('public_id', $c)->delete();
        }

        Session::flash('flash_message', 'Candidatos removidos em massa com sucesso!');
        return redirect()->back();
    }
}
