<?php

namespace App\Http\Controllers\Painel;

use App\Models\Vaga;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateVagaRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VagasControler extends Controller
{
    protected $view = 'vagas.';

    public function index(Request $request)
    {
        $order   = $request->filled('order') ? $request->order : 'cargo';
        $orderby = $request->filled('orderby') ? $request->orderby : 'asc';
        $vagas   = Vaga::when($request->filled('pesquisar'), function($query) use ($request){
            return $query->where('cargo', 'LIKE', '%'.$request->pesquisar.'%')
                ->orWhere('contratacao', 'LIKE', '%'.$request->pesquisar.'%')
                ->orWhere('public_id', 'LIKE', '%'.$request->pesquisar.'%');
        })->orderBy($order, $orderby)->paginate(20);
        return view($this->view . 'index', compact('vagas'));
    }


    public function create()
    {
        return view($this->view . 'form');
    }


    public function store(CreateVagaRequest $request)
    {
        Vaga::create( $request->all() );
        Session::flash('flash_message', 'Vaga salva com sucesso!');
        return redirect()->route('painel.' . $this->view . 'index');
    }


    public function update(CreateVagaRequest $request, Vaga $vaga)
    {
        $vaga->update( $request->all() );
        Session::flash('flash_message', 'Vaga atualizada com sucesso!');
        return redirect()->route('painel.' . $this->view . 'index');
    }


    public function updateStatus(Vaga $vaga)
    {
        $vaga->update(['status' => ($vaga->status == 0 ? 1 : 0)]);
        Session::flash('flash_message', 'Status de vaga atualizado com sucesso!');
        return redirect()->back();
    }


    public function edit(Vaga $vaga)
    {
        return view($this->view . 'form', compact('vaga'));
    }
 

    public function destroy(Vaga $vaga)
    {
        $vaga->delete();
        Session::flash('flash_message', 'Vaga removida com sucesso!');
        return redirect()->route('painel.' . $this->view . 'index');
    }
 

    public function destroyMass(Request $request)
    {
        $checados = explode(',', trim($request->checados, ','));

        foreach($checados as $c) {
            Vaga::where('public_id', $c)->delete();
        }

        Session::flash('flash_message', 'Vagas removidas em massa com sucesso!');
        return redirect()->back();
    }
}
