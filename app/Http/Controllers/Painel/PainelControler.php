<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\Candidato;
use App\Models\Vaga;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PainelControler extends Controller
{
    public function index()
    {
        $vagas = Vaga::count();
        $candidatos = Candidato::count();
        return view('dashboard', compact('vagas', 'candidatos'));
    }
}
