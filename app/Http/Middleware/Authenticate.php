<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        switch($request->route()->getPrefix()){
            case 'candidato' : $route = 'candidato.login'; break;
            default          : $route = 'login'; break;
        }

        if (! $request->expectsJson()) {
            return route($route);
        }
        
    }
}
