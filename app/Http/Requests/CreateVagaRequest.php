<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateVagaRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if (!$this->filled('status'))
            $this->merge(['status' => 0]);
        
        if (!$this->filled('contratacao'))
            $this->merge(['contratacao' => 0]);

        return [
            'cargo' => 'required|string|max:191',
        ];
    }

    public function messages()
    {
        return [
            'cargo.*' => 'O cargo é obrigatório e precisa conter no máximo 191 caracteres',
        ];
    }
    
}
