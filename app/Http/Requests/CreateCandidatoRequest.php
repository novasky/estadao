<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCandidatoRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if (!$this->filled('status'))
            $this->merge(['status' => 0]);

        return [
            'nome'  => 'required|string|max:191',
            'email' => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            'nome.*'  => 'Você precisa informar o seu nome',
            'email.*' => 'O seu e-mail precisa ser válido',
        ];
    }
    
}
