<?php

/**
 * Parâmetros pré-fixados para o uso no sistema
 */

return [
    
    'contratacao' => [
        0 => 'CLT',
        1 => 'Pessoa Jurídica',
        2 => 'Freelancer',
    ],
    
    'status' => [
        0 => 'Inativo / Pausado',
        1 => 'Ativo',
    ],

];