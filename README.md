# Teste para candidatos à vaga de Desenvolvedor Php / Laravel

Esta é uma aplicação web utilizando o framework PHP Laravel para a construção de um sistema básico de anúncio de vagas de emprego com gerenciamento de candidatos.

### Instalação

Após clonar o branch, instalar as depências via composer e criar o arquivo .env, deve-se informar um banco de dados e rodar os comandos:

```
php artisan key:generate
php artisan migrate
php artisan db:seed
```

### Rodando a Aplicação
```
php artisan serve
```

Através da URL http://localhost:8000/ será possível visualizar as vagas. Aqui o usuário também pode se candidatar para as vagas, bem como se cadastrar no sistema também.

### Administração / CMS
O painel administrativo estará disponível em http://localhost:8000/login e nele há definido 02 usuários fixos. Ambos estão disponíveis para consulta em *database/seeders/UsersTableSeeder.php*

Pode-se utilizar os dados:
Usuário: mecontrata@estadao.com.br
Senha: admin123

### API
Há uma API REST disponível em http://localhost:8000/api/v1/
Para TODAS as requisições será necessário enviar o token do usuário como parâmetro da URL: **?api_token=**
O token pode ser consultado na dashboard do painel administrativo.

#### APIS Disponíveis:
- /candidatos
- /vagas

Todos os métodos CRUD estão disponíveis para **Gerenciamento de Candidatos**:
|                |URL| Parâmetros|
|----------------|-------------------------------|-----------------------------|
|GET|`/candidatos?api_token=`            |           |
|GET|`/candidatos/{id_candidato}?api_token=`            |            |
| POST |`/candidatos?api_token=`|nome - email - telefone - password|
| POST |`/candidatos/{id_candidato}?api_token=`|nome - email - telefone - password - _method(Valor: PUT) |
| DELETE|`/candidatos/{id_candidato}?api_token=`||

---
Todos os métodos CRUD estão disponíveis para **Gerenciamento de Vagas**:
|                |URL| Parâmetros|
|----------------|-------------------------------|-----------------------------|
|GET|`/vagas?api_token=`            |           |
|GET|`/vagas/{id_vaga}?api_token=`            |            |
| POST |`/vagas?api_token=`|cargo - descricao - contratacao (Valor: 0=CLT; 1=PessoaFísica; 2=Freelancer)|
| POST |`/vagas/{id_vaga}?api_token=`|cargo - descricao - contratacao - _method(Valor: PUT) |
| DELETE|`/vagas/{id_vaga}?api_token=`||


