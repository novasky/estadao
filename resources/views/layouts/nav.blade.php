@if (Route::has('candidato.login'))
    <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block main-nav">
        @auth()
            <a href="{{ route('painel.vagas.index') }}" class="text-sm text-gray-700 underline">Vagas</a>
            <a href="{{ route('painel.candidatos.index') }}" class="text-sm text-gray-700 underline">Candidatos</a>
        @else
            @auth('candidato')
                <a href="{{ route('home') }}" class="text-sm text-gray-700 underline">Vagas</a>
                <a href="{{ route('candidato.candidaturas') }}" class="text-sm text-gray-700 underline">Minhas Candidaturas</a>
            @else
                <a href="{{ route('candidato.login') }}" class="text-sm text-gray-700 underline">Entrar</a>

                @if (Route::has('register'))
                    <a href="{{ route('candidato.register') }}" class="ml-4 text-sm text-gray-700 underline">Quero me Cadastrar</a>
                @endif
            @endauth
        @endauth

        @auth()
            <a class="float-end" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Sair
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        @else
            @auth('candidato')
                <a class="float-end" href="{{ route('candidato.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Sair
                </a>

                <form id="logout-form" action="{{ route('candidato.logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-700 underline float-end">Administrativo</a>
            @endauth
        @endauth
    </div>
@endif