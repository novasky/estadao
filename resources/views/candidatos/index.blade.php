@extends('layouts.template')

@section('maincontent')
<h2 class="">Gerenciamento de Candidatos</h2>

@if(Session::has('flash_message'))
    <div class="alert alert-success mb-5" role="alert">
        {{ Session::get('flash_message') }}
    </div>
@endif

<div class="container">
    <div class="row mb-5 mt-4">
        <div class="col">
            <form action="{{ route('painel.candidatos.index') }}" method="get" class="row g-3 float-start">
                <div class="col-auto">
                  <label for="pesquisar" class="visually-hidden">Pesquisar...</label>
                  <input type="text" class="form-control" name="pesquisar" value="{{ request('pesquisar') }}" placeholder="Pesquisar por nome...">
                </div>
                <div class="col-auto">
                  <button type="submit" class="btn btn-primary mb-3">Pesquisar</button>
                </div>
            </form>
        </div>
    </div>


    <a href="#" class="btn btn-outline-danger __action_remover"><i class="bi bi-trash"></i> Remover Selecionados</a>
    <form method="post" class="__form_remover" action="{{ route('painel.candidatos.destroy-mass') }}">
        @csrf
        @method('DELETE')
        <input type="hidden" name="checados" class="checados" value="">
    </form>


    <table class="table table-hover">
        <thead>
            <tr>
                <td scope="col">&nbsp;</td>
                <td scope="col"><a href="{{ route('painel.candidatos.index', ['pesquisar'=>request('pesquisar'), 'order' => 'public_id', 'orderby' => (strtolower(request('orderby')) == 'asc' ? 'desc' : 'asc')]) }}">ID</a></td>
                <td scope="col"><a href="{{ route('painel.candidatos.index', ['pesquisar'=>request('pesquisar'), 'order' => 'nome', 'orderby' => (strtolower(request('orderby')) == 'asc' ? 'desc' : 'asc')]) }}">Nome</a></td>
                <td scope="col"><a href="{{ route('painel.candidatos.index', ['pesquisar'=>request('pesquisar'), 'order' => 'email', 'orderby' => (strtolower(request('orderby')) == 'asc' ? 'desc' : 'asc')]) }}">E-mail</a></td>
                <td scope="col"><a href="{{ route('painel.candidatos.index', ['pesquisar'=>request('pesquisar'), 'order' => 'telefone', 'orderby' => (strtolower(request('orderby')) == 'asc' ? 'desc' : 'asc')]) }}">Telefone</a></td>
                <td scope="col">N. Candidaturas</td>
                <td scope="col">&nbsp;</td>
                <td scope="col">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
        @forelse ($candidatos as $candidato)
            <tr>
                <td scope="row">
                    <input type="checkbox" name="candidatos[]" value="{{ $candidato->public_id }}" class="__checks">
                </td>
                <td>{{ $candidato->public_id }}</td>
                <td>{{ $candidato->nome }}</td>
                <td>{{ $candidato->email }}</td>
                <td>{{ $candidato->telefone }}</td>
                <td>{{ $candidato->candidaturas->count() }} Candidatura{{ $candidato->candidaturas->count() != 1 ? 's' : '' }}</td>
                <td>
                    <a href="{{ route('painel.candidatos.edit', $candidato->public_id) }}" class="btn btn-light btn-small">Editar</a>
                </td>
                <td>
                    <form action="{{ route('painel.candidatos.destroy', $candidato->public_id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-small">Excluir</button>
                    </form>
                </td>
            </tr>
        @empty
        <tr>
            <td colspan="8">
                Nenhuma candidato cadastrado até o momento!
            </td>
        </tr>
        @endforelse
        </tbody>
    </table>

    <div class="row justify-content-center">
        <div class="col">
            {{ $candidatos->appends(['pesquisar'=>request('pesquisar'), 'order'=>request('order'), 'orderby'=>request('orderby')])->links() }}
        </div>
    </div>
</div>
@endsection