@extends('layouts.template')

@section('maincontent')
<h2 class="">Gerenciamento de Candidatos</h2>

@if(Session::has('flash_message'))
    <div class="alert alert-success mb-5" role="alert">
        {{ Session::get('flash_message') }}
    </div>
@endif

<div class="container">
    <div class="row mb-5">
        <div class="col">
            <a href="{{ route('painel.candidatos.index') }}" class="btn btn-secondary btn-small float-end"><i class="bi bi-arrow-left-short"></i> Voltar</a>
        </div>
    </div>
    
    <form action="{{ isset($candidato) ? route('painel.candidatos.update', $candidato->public_id) : route('painel.candidatos.store') }}" class="row justify-content-md-center needs-validation" method="post">
        {{ isset($candidato) ? method_field('PUT') : '' }}
        @csrf

        <div class="col-6">
            @if ($errors->any())
                <ul class="row mb-4 list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item list-group-item-danger">{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            
            <div class="mb-3 row">
                <label for="nome" class="col-sm-2 col-form-label">Nome</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nome" value="{{ isset($candidato) ? $candidato->nome : old('nome') }}" required>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="email" class="col-sm-2 col-form-label">E-mail</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="email" value="{{ isset($candidato) ? $candidato->email : old('email') }}" required>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="telefone" class="col-sm-2 col-form-label">Telefone</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="telefone" value="{{ isset($candidato) ? $candidato->telefone : old('telefone') }}" required>
                </div>
            </div>

            <div class="mt-5 mb-3 row">
                <div class="col">
                    <h4>Candidaturas em andamento:</h4>
                    <ul>
                        @forelse ($candidato->candidaturas as $candidatura)
                            <li>&raquo; <a href="{{ route('painel.vagas.edit', $candidatura->public_id) }}" target="_blank">{{ $candidatura->cargo }}</a></li>
                        @empty
                            <li>
                                <small>Nenhuma candidatura até o momento!</small>
                            </li>
                        @endforelse
                    </ul>
                </div>
            </div>

            <div class="row">
                <button class="btn btn-primary" type="submit">Salvar</button>
            </div>
        </div>
    </form>
</div>
@endsection