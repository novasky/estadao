@extends('layouts.template')

@section('maincontent')
<h1 class="mb-5">Bem vindo ao seu painel administrativo!</h1>

@if(Session::has('flash_message'))
    <div class="alert alert-success mb-5" role="alert">
        {{ Session::get('flash_message') }}
    </div>
@endif

<div class="container">

    <div class="row mb-5">
        <div class="col">

            <div class="alert alert-primary d-flex align-items-center" role="alert">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                  <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                </svg>
                <div>
                    Seu token para API é: <strong>{{ auth()->user()->api_token }}</strong>
                </div>
              </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="card mb-2">
                <div class="card-body">
                    <h5 class="card-title">Candidatos</h5>
                    <h1 class="text-center text-success">{{ $candidatos }}</h1>
                    <p class="card-text text-center">Cadastrados</p>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card mb-2">
                <div class="card-body">
                    <h5 class="card-title">Vagas</h5>
                    <h1 class="text-center text-success">{{ $vagas }}</h1>
                    <p class="card-text text-center">Cadastradas</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection