<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>
    </head>
    <body>
        <div class="container">

            <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
                @include('layouts.nav')
                
                <h1 class="mb-5">Vagas em Aberto</h1>

                @if(Session::has('flash_message'))
                    <div class="alert alert-success mb-5" role="alert">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="container">
                    <ul>
                        @forelse ($vagas as $vaga)
                            <li>
                                <h3 class="{{ auth()->guard('candidato')->check() && $vaga->candidatos->contains( auth()->guard('candidato')->user()->id ) ? 'text-muted' : '' }}">{{ $vaga->cargo }}</h3>
                                <p>{{ $vaga->descricao }}</p>
                                <p>
                                    @if($vaga->status == 1)
                                        @if( auth()->guard('candidato')->check() && $vaga->candidatos->contains( auth()->guard('candidato')->user()->id ) )
                                            <span class="badge rounded-pill bg-light text-dark">Você já se candidatou</span>
                                        @else
                                            <form action="{{ route('candidato.candidatar', $vaga->public_id) }}" method="post">
                                                @csrf
                                                <button type="submit" class="btn btn-sm btn-dark">Quero me candidatar</button>
                                            </form>
                                        @endif
                                    @else
                                        <span class="badge rounded-pill bg-danger">Vaga indisponível</span>
                                    @endif
                                </p>
                                <hr>
                            </li>
                        @empty
                            <li>
                                <h4>Nenhuma vaga disponível!</h4>
                            </li>
                        @endforelse
                    </ul>

                    {{ $vagas->links() }}
                </div>
            </div>
        </div>
    </body>
</html>
