@extends('layouts.template')

@section('maincontent')
<h2 class="">Gerenciamento de Vagas</h2>

@if(Session::has('flash_message'))
    <div class="alert alert-success mb-5" role="alert">
        {{ Session::get('flash_message') }}
    </div>
@endif

<div class="container">
    <div class="row mb-5">
        <div class="col">
            <a href="{{ route('painel.vagas.index') }}" class="btn btn-secondary btn-small float-end"><i class="bi bi-arrow-left-short"></i> Voltar</a>
        </div>
    </div>
    
    <form action="{{ isset($vaga) ? route('painel.vagas.update', $vaga->public_id) : route('painel.vagas.store') }}" class="row justify-content-md-center needs-validation" method="post">
        {{ isset($vaga) ? method_field('PUT') : '' }}
        @csrf

        <div class="col-6">
            @if ($errors->any())
                <ul class="row mb-4 list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item list-group-item-danger">{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            
            <div class="mb-3 row">
                <label for="cargo" class="col-sm-2 col-form-label">Cargo</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="cargo" value="{{ isset($vaga) ? $vaga->cargo : old('cargo') }}" required>
                </div>
            </div>

            <div class="mb-3 row">
                <label for="contratacao" class="col-sm-2 col-form-label">Modelo de Contratação</label>
                <div class="col-sm-10">
                  <select name="contratacao" id="contratacao" class="form-select">
                      @foreach (config('parameters.contratacao') as $cont_id => $cont_nome)
                          <option value="{{ $cont_id }}" {{ isset($vaga) && $vaga->contratacao == $cont_id || old('contratacao') == $cont_id ? 'selected' : '' }}>{{ $cont_nome }}</option>
                      @endforeach
                  </select>
                </div>
            </div>

            <div class="mb-3 row">
                <label for="descricao" class="col-sm-2 col-form-label">Descrição da Vaga</label>
                <div class="col-sm-10">
                  <textarea name="descricao" class="form-control" cols="30" rows="10">{!! isset($vaga) ? $vaga->descricao : old('descricao') !!}</textarea>
                </div>
            </div>

            <div class="row">
                <button class="btn btn-primary" type="submit">Salvar</button>
            </div>
        </div>
    </form>
</div>
@endsection