@extends('layouts.template')

@section('maincontent')
<h2 class="">Gerenciamento de Vagas</h2>

@if(Session::has('flash_message'))
    <div class="alert alert-success mb-5" role="alert">
        {{ Session::get('flash_message') }}
    </div>
@endif

<div class="container">
    <div class="row mb-5 mt-4">
        <div class="col">
            <form action="{{ route('painel.vagas.index') }}" method="get" class="row g-3 float-start">
                <div class="col-auto">
                  <label for="pesquisar" class="visually-hidden">Pesquisar...</label>
                  <input type="text" class="form-control" name="pesquisar" value="{{ request('pesquisar') }}" placeholder="Pesquisar por nome...">
                </div>
                <div class="col-auto">
                  <button type="submit" class="btn btn-primary mb-3">Pesquisar</button>
                </div>
            </form>
            <a href="{{ route('painel.vagas.create') }}" class="btn btn-success btn-small float-end"><i class="bi bi-plus"></i> Adicionar</a>
        </div>
    </div>


    <a href="#" class="btn btn-outline-danger __action_remover"><i class="bi bi-trash"></i> Remover Selecionados</a>
    <form method="post" class="__form_remover" action="{{ route('painel.vagas.destroy-mass') }}">
        @csrf
        @method('DELETE')
        <input type="hidden" name="checados" class="checados" value="">
    </form>
    

    <table class="table table-hover">
        <thead>
            <tr>
                <td scope="col">&nbsp;</td>
                <td scope="col"><a href="{{ route('painel.vagas.index', ['pesquisar'=>request('pesquisar'), 'order' => 'public_id', 'orderby' => (strtolower(request('orderby')) == 'asc' ? 'desc' : 'asc')]) }}">ID</a></td>
                <td scope="col"><a href="{{ route('painel.vagas.index', ['pesquisar'=>request('pesquisar'), 'order' => 'cargo', 'orderby' => (strtolower(request('orderby')) == 'asc' ? 'desc' : 'asc')]) }}">Cargo</a></td>
                <td scope="col">N. Candidatos</td>
                <td scope="col"><a href="{{ route('painel.vagas.index', ['pesquisar'=>request('pesquisar'), 'order' => 'contratacao', 'orderby' => (strtolower(request('orderby')) == 'asc' ? 'desc' : 'asc')]) }}">Contratação</a></td>
                <td scope="col"><a href="{{ route('painel.vagas.index', ['pesquisar'=>request('pesquisar'), 'order' => 'status', 'orderby' => (strtolower(request('orderby')) == 'asc' ? 'desc' : 'asc')]) }}">Status</a></td>
                <td scope="col">&nbsp;</td>
                <td scope="col">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
        @forelse ($vagas as $vaga)
            <tr>
                <td scope="row">
                    <input type="checkbox" name="vagas[]" value="{{ $vaga->public_id }}" class="__checks">
                </td>
                <td>{{ $vaga->public_id }}</td>
                <td>{{ $vaga->cargo }}</td>
                <td>{{ $vaga->candidatos->count() }} Candidato{{ $vaga->candidatos->count() != 1 ? 's' : '' }}</td>
                <td>{{ config('parameters.contratacao.' . (int)$vaga->contratacao) }}</td>
                <td>
                    <a href="{{ route('painel.vagas.update-status', $vaga->public_id) }}" class="badge bg-{{ (int)$vaga->status == 0 ? 'warning' : 'success' }}">{{ config('parameters.status.' . (int)$vaga->status) }}</a>
                </td>
                <td>
                    <a href="{{ route('painel.vagas.edit', $vaga->public_id) }}" class="btn btn-info btn-small">Editar</a>
                </td>
                <td>
                    <form action="{{ route('painel.vagas.destroy', $vaga->public_id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-small">Excluir</button>
                    </form>
                </td>
            </tr>
        @empty
        <tr>
            <td colspan="6">
                Nenhuma vaga cadastrada no momento
            </td>
        </tr>
        @endforelse
        </tbody>
    </table>

    <div class="row justify-content-center">
        <div class="col">
            {{ $vagas->appends(['pesquisar'=>request('pesquisar'), 'order'=>request('order'), 'orderby'=>request('orderby')])->links() }}
        </div>
    </div>
</div>
@endsection