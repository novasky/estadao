@extends('candidato.auth.base')

@section('content')
<div class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-8 col-lg-6 col-xl-5 mt-5">

                <form method="POST" action="{{ route('candidato.login') }}">
                    {{ csrf_field() }}

                    <div class="card p-5 shadow-sm">
                        <div class="form-row">

                            <div class="form-group col-md-12 text-center">
                                FAÇA SEU LOGIN
                            </div>

                            <div class="form-group col-md-12 text-center my-3">
                                <h6 class="text-secondary">Informe seu email e senha para continuar.</h6>
                            </div>

                            <div class="mb-3">
                                <label for="email">E-mail</label>
                                <input id="email" name="email" type="text" class="form-control {{ $errors->has('email') ? 'border-danger' : '' }}" value="{{ old('email') ?: old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <small class="text-danger">{{ $errors->first('email') ?: $errors->first('email') }}</small>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="password">Senha</label>
                                <input id="password" type="password" class="form-control {{ $errors->has('password') ? 'border-danger' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                    <small class="text-danger">{{ $errors->first('password') }}</small>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Lembrar meus dados
                                </label>
                            </div>

                            <div class="form-group col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Entrar</button>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-12 text-center">
                                <hr>
                                <span class="text-muted">Ainda não tem uma conta?</span>
                            </div>

                            <div class="form-group col-md-12 text-center">
                                <a href="{{ route('candidato.register') }}" class="btn btn-secondary">Criar conta grátis</a>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection
