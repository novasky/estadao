@extends('candidato.auth.base')

@section('content')
    <div class="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-8 col-lg-6 col-xl-5 mt-5">

                    @if ( session('status') )
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('candidato.password.email') }}">
                        {{ csrf_field() }}

                        <div class="card p-5 shadow-sm">
                            <div class="form-row">

                                <div class="form-group col-md-12 text-center">
                                    
                                </div>

                                <div class="form-group col-md-12 text-center my-3">
                                    <h4 class="text-secondary">Redefinir Senha</h4>
                                    {{-- <h6 class="text-secondary">Enviaremos uma nova senha para seu email.</h6> --}}
                                </div>

                                <div class="form-group col-md-12 {{ $errors->has('email') ? 'border-danger' : '' }}">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail cadastrado" required>
                                    @if ($errors->has('email'))
                                        <small class="text-danger">{{ $errors->first('email') }}</small>
                                    @endif
                                </div>

                                <div class="form-group col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Redefinir senha</button>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
