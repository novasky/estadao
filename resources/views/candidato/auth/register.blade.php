@extends('candidato.auth.base')

@section('content')
<div class="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-8 col-lg-6 col-xl-5 mt-5">

                <form method="POST" action="{{ route('candidato.register', (isset($indicacao) && $indicacao ? ['indicacao' => $indicacao] : null)) }}">
                    {{ csrf_field() }}

                    <div class="card p-5 shadow-sm">
                        <div class="form-row">

                            <div class="form-group col-md-12 text-center">
                                Cadastre-se gratuitamente
                            </div>

                            <div class="form-group col-md-12 text-center my-3">
                                {{-- <h6 class="text-secondary"></h6> --}}
                            </div>

                            @if ($errors->any())
                                <div class="col-md-12 my-3">
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            &raquo; {{ $error }}<br>
                                        @endforeach
                                    </div>
                                </div>
                            @endif

                            @if(isset($indicacao) && $indicacao)
                                <div class="mb-3 my-3">
                                    Indicação de: <strong>{{ $indicacao->aluno->nome }}</strong>.
                                </div>
                            @endif

                            <div class="mb-3">
                                <label for="nome">Nome Completo</label>
                                <input id="nome" type="text" class="form-control {{ $errors->has('nome') ? 'border-danger' : '' }}" name="nome" value="{{ old('nome') }}" required autofocus>
                                @if ($errors->has('nome'))
                                    <small class="text-danger">{{ $errors->first('nome') }}</small>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="email">E-Mail</label>
                                <input id="email" type="email" class="form-control {{ $errors->has('email') ? 'border-danger' : '' }}" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <small class="text-danger">{{ $errors->first('email') }}</small>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="telefone">Telefone</label>
                                <input id="telefone" type="text" class="form-control mask-phone {{ $errors->has('telefone') ? 'border-danger' : '' }}" name="telefone" value="{{ old('telefone') }}" required>
                                @if ($errors->has('telefone'))
                                    <small class="text-danger">{{ $errors->first('telefone') }}</small>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="password">Senha</label>
                                <input id="password" type="password" class="form-control {{ $errors->has('password') ? 'border-danger' : '' }}" name="password" required autocomplete="new-password">
                                @if ($errors->has('password'))
                                    <small class="text-danger">{{ $errors->first('password') }}</small>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="password_confirmation">Repita sua Senha</label>
                                <input id="password_confirmation" type="password" class="form-control {{ $errors->has('password_confirmation') ? 'border-danger' : '' }}" name="password_confirmation" required autocomplete="new-password">
                                @if ($errors->has('password_confirmation'))
                                    <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                                @endif
                            </div>

                            <div class="mb-3 text-center">
                                <button type="submit" class="btn btn-primary">Avançar</button>
                            </div>

                            <div class="mb-3 text-center">
                                <a class="btn btn-link" href="{{ route('candidato.login') }}">Já tem cadastro?</a>
                            </div>

                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection
