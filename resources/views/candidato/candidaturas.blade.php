<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>
    </head>
    <body>
        <div class="container">

            <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
                @include('layouts.nav')

                <h1 class="mb-5">Minhas Candidaturas</h1>

                @if(Session::has('flash_message'))
                    <div class="alert alert-success mb-5" role="alert">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="container">
                    <ul>
                        @forelse ($candidaturas as $candidatura)
                            <li>
                                <h3>{{ $candidatura->cargo }}</h3>
                                <p>{{ $candidatura->descricao }}</p>
                                <p>
                                    {{ $candidatura->created_at->diffForHumans() }}
                                </p>
                                <hr>
                            </li>
                        @empty
                            <li>
                                <h4>Você ainda não se candidatou</h4>
                            </li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
