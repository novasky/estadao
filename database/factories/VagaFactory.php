<?php

namespace Database\Factories;

use App\Models\Vaga;
use Illuminate\Database\Eloquent\Factories\Factory;

class VagaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vaga::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'public_id'   => rand(9999, 999999999),
            'cargo'       => $this->faker->sentence($nbWords = 4, $variableNbWords = true),
            'descricao'   => $this->faker->paragraphs(3, true),
            'contratacao' => rand(0,2),
            'status'      => 1,
            'created_at'  => now(),
            'updated_at'  => now(),
        ];
    }
}
