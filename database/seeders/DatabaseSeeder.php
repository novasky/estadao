<?php

namespace Database\Seeders;

use App\Models\Vaga;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();

        $this->call(UsersTableSeeder::class);
        Vaga::factory(30)->create();
    }
}
