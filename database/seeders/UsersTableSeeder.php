<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'      => 'William Novasky' ,
            'email'     => 'wnovasky@gmail.com' ,
            'password'  => bcrypt('admin123') ,
            'api_token' => Str::random(60) ,
            'created_at' => \Carbon\Carbon::now() ,
            'updated_at' => \Carbon\Carbon::now() ,
        ]);

        DB::table('users')->insert([
            'name'      => 'Estadão - Grupo Estado' ,
            'email'     => 'mecontrata@estadao.com.br' ,
            'password'  => bcrypt('admin123') ,
            'api_token' => Str::random(60) ,
            'created_at' => \Carbon\Carbon::now() ,
            'updated_at' => \Carbon\Carbon::now() ,
        ]);
    }
}
