<?php

use App\Models\Candidato;
use App\Models\Vaga;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| http://localhost:8000/api/v1?api_token={token}
|
*/

Route::prefix('v1')->middleware('auth:api')->group(function(){
    Route::get('/', function(Request $request){
        return response()->json(['status' => 'success', 'user' => $request->user()->name]);
    });
    

    Route::prefix('vagas')->group(function(){
        Route::get('/', function(){
            return Vaga::all()->map->only(['public_id', 'cargo', 'contratacao', 'descricao', 'status', 'created_at']);
        });
        Route::get('/{vaga}', function(Vaga $vaga){
            return $vaga;
        });
        Route::post('/', function(Request $request){
            if(!$request->filled('cargo')) {
                return response()->json(['error' => 'Você precisa informar o nome do cargo']);
            }
            return Vaga::create($request->only(['cargo', 'contratacao', 'descricao', 'status']))
                ->only(['public_id', 'cargo', 'contratacao', 'descricao', 'status', 'created_at']);
        });
        Route::put('/{vaga}', function(Request $request, Vaga $vaga){
            if(!$request->filled('cargo')) {
                return response()->json(['error' => 'Você precisa informar o nome do cargo']);
            }
            $vaga->update($request->only(['cargo', 'contratacao', 'descricao', 'status']));

            return $vaga->only(['public_id', 'cargo', 'contratacao', 'descricao', 'status', 'created_at']);
        });
        Route::delete('/{vaga}', function(Vaga $vaga){
            return $vaga->delete();
        });
    });

    

    Route::prefix('candidatos')->group(function(){
        Route::get('/', function(){
            return Candidato::all()->map->only(['public_id', 'nome', 'email', 'telefone', 'created_at']);
        });
        Route::get('/{candidato}', function(Candidato $candidato){
            return $candidato;
        });
        Route::post('/', function(Request $request){
            if(!$request->filled('nome')) {
                return response()->json(['error' => 'Você precisa informar o nome do candidato']);
            }
            if(!$request->filled('email')) {
                return response()->json(['error' => 'Você precisa informar o e-mail do candidato']);
            }
            if(!$request->filled('password')) {
                return response()->json(['error' => 'Você precisa informar a senha do candidato']);
            }

            $inputs = $request->only(['nome', 'email', 'telefone', 'password']);
            $inputs['password'] = bcrypt($inputs['password']);
            return Candidato::create($inputs)
                ->only(['public_id', 'nome', 'email', 'telefone', 'created_at']);
        });
        Route::put('/{candidato}', function(Request $request, Candidato $candidato){
            if(!$request->filled('nome')) {
                return response()->json(['error' => 'Você precisa informar o nome do candidato']);
            }
            if(!$request->filled('email')) {
                return response()->json(['error' => 'Você precisa informar o e-mail do candidato']);
            }
            $candidato->update($request->only(['nome', 'email', 'telefone']));

            return $candidato->only(['public_id', 'nome', 'email', 'telefone', 'created_at']);
        });
        Route::delete('/{candidato}', function(Candidato $candidato){
            return $candidato->delete();
        });
    });
});
