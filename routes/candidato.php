<?php

use App\Http\Controllers\CandidatoController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:candidato'])->group(function () {
    Route::post('/vaga/{public_id}', [CandidatoController::class, 'candidatar'])->name('candidatar');
    Route::get('/candidaturas', [CandidatoController::class, 'candidaturas'])->name('candidaturas');
});

Auth::routes(['guard' => 'candidato']);