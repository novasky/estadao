<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\Painel\CandidatosControler;
use App\Http\Controllers\Painel\VagasControler;
use App\Http\Controllers\Painel\PainelControler;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::prefix('painel')->middleware(['auth'])->name('painel.')->group(function () {
    Route::get('/', [PainelControler::class, 'index'])->name('home');
    Route::get('/vagas/update-status/{vaga}', [VagasControler::class, 'updateStatus'])->name('vagas.update-status');
    Route::delete('/vagas', [VagasControler::class, 'destroyMass'])->name('vagas.destroy-mass');
    Route::resource('vagas', VagasControler::class, ['names'=>'vagas']);

    Route::delete('/candidatos', [CandidatosControler::class, 'destroyMass'])->name('candidatos.destroy-mass');
    Route::resource('candidatos', CandidatosControler::class, ['names'=>'candidatos']);
});


Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
